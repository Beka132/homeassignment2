package com.example.a2


    fun numberToWord(number:Int):String{        //ფუნქცია

        var sityva="sityva"         // ცვლადი რომელსაც დააბრუნებს ეს ფუნქცია

        val ataseuli="ათასი"
        val aseulebi = arrayOf("","ას","ორას","სამას","ოთხას","ხუთას","ექვსას","შვიდას","რვაას","ცხრაას")           //ერეიები, რომლებიც იქნება გამოყენებული რიცხვის ასეულებად, ათეულებად ან ერთეულებად
        val ateulebi= arrayOf("","ოც","ორმოც","სამოც","ოთხმოც")
        val ateulebiGrdzeli= arrayOf("","","ოც","ოც","ორმოც","ორმოც","სამოც","სამოც","ოთხმოც","ოთხმოც")
        val ati= arrayOf("ათი","თერთმეტი","თორმეტი","ცამეტი","თოთხმეტი","თხუთმეტი","თექვსმეტი","ჩვიდმეტი","თვრამეტი","ცხრამეტი")
        val kentiAteulebisErteulebi= arrayOf("დაათი","დათერთმეტი","დათორმეტი","დაცამეტი","დათოთხმეტი","დათხუთმეტი","დათექვსმეტი","დაჩვიდმეტი","დათვრამეტი","დაცხრამეტი")
        val luwiAteulebisErteulebi= arrayOf("ი","დაერთი","დაორი","დასამი","დაოთხი","დახუთი","დაექვსი","დაშვიდი","დარვა","დაცხრა")
        val erteulebi = arrayOf("","ერთი","ორი","სამი","ოთხი","ხუთი","ექვსი","შვიდი","რვა","ცხრა")

        if(number<10){              //სხვადასხვა შემთხვევისათვის არის გამოყენებული if-Else ბრძანებები

            sityva=erteulebi[number]

        } else if(number<100){

            if ((number/10)%2==0){

                sityva=ateulebi[(number/10)/2]+luwiAteulebisErteulebi[number%10]

            }else if ((number/10)%2!=0){

                if(number/10==1){

                    sityva=ati[number%10]
                    return sityva

                }

                sityva=ateulebi[(number/10)/2]+kentiAteulebisErteulebi[number%10]

            }

        } else if(number<1000){

            if (number%100==0){

                sityva=aseulebi[number/100]+luwiAteulebisErteulebi[number%100]

                return sityva

            }

            if ((number/10)%10==0) {

                sityva = aseulebi[number/100]+erteulebi[number%10]

                return sityva

            } else if ((number/10)%10==1){

                sityva=aseulebi[number/100]+ati[number%10]

                return sityva

            }
            if (number%100==0) sityva=aseulebi[number/100]+"ი"

            else{

                sityva=aseulebi[number/100]

                if(((number/10)%10)%2==0) sityva+= ateulebi[((number/10)%10)/2]+ luwiAteulebisErteulebi[number%10]

                if (((number/10)%10)%2!=0){

                    sityva+=ateulebiGrdzeli[(number/10)%10]+kentiAteulebisErteulebi[number%10]

                }

            }

        }else if(number==1000){

            return ataseuli

        } else {    // იმ შემთხვევაში თუ რიცხვი მეტია 1000-ზე ან ნაკლებია 1-ზე

            return "მოცემული რიცხვი არ შედის 1-1000 დიაპაზონში"

        }

        return sityva
    }



fun main() {


        for(x in 1..1000){          //ყველა რიცხვის შესამოწმებლად
            println(numberToWord(x))
        }



}

